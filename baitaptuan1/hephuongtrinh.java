import javax.swing.JOptionPane;
public class hephuongtrinh{
    public static void main(String[] args){
        String stra1, stra2, strb1, strb2, strc1, strc2;
        double a1, a2, b1, b2, c1, c2;
        stra1 = JOptionPane.showInputDialog(null , "Pls input a1:", JOptionPane.INFORMATION_MESSAGE);
        a1= Double.parseDouble(stra1);
        strb1 = JOptionPane.showInputDialog(null , "Pls input b1:", JOptionPane.INFORMATION_MESSAGE);
        b1= Double.parseDouble(strb1);
        strc1 = JOptionPane.showInputDialog(null , "Pls input c1:", JOptionPane.INFORMATION_MESSAGE);
        c1= Double.parseDouble(strc1);

        stra2 = JOptionPane.showInputDialog(null , "Pls input a2:", JOptionPane.INFORMATION_MESSAGE);
        a2= Double.parseDouble(stra2);
        strb2 = JOptionPane.showInputDialog(null , "Pls input b2:", JOptionPane.INFORMATION_MESSAGE);
        b2= Double.parseDouble(strb2);
        strc2 = JOptionPane.showInputDialog(null , "Pls input c2:", JOptionPane.INFORMATION_MESSAGE);
        c2= Double.parseDouble(strc2);

        double D = a1 * b2 - a2 * b1;
        double D1 = c1*b2 - c2*b1;
        double D2 = c2*a1 - c1*a2;
        if(D== 0){
                if(D == D1 && D == D2)
                JOptionPane.showMessageDialog(null, "There is no solution for the equation !");
                else JOptionPane.showMessageDialog(null, "There are infinite solutions for the equation !");
            }
        else {
                double x1 = D1/D;
                double x2 = D2/D;
                JOptionPane.showMessageDialog(null, "The equation has 2 solutions : x1 = " + x1 + " and x2 = " + x2);
            }
        }
}
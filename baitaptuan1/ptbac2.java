import java.lang.Math;
import javax.swing.JOptionPane;

public class ptbac2 {
    public static void main(String[] args){
        String stra1, stra2, stra3;
        double a, b, c, del, x1, x2;
        stra1 = JOptionPane.showInputDialog(null, "Moi ban nhap a :", JOptionPane.INFORMATION_MESSAGE);
        a = Double.parseDouble(stra1);

        while(a == 0) {
            JOptionPane.showMessageDialog(null, "a khong hop le, moi ban nhap lai !");
            stra1 = JOptionPane.showInputDialog(null , "moi ban nhap a :", JOptionPane.INFORMATION_MESSAGE);
            a = Double.parseDouble(stra1);
        }

        stra2 = JOptionPane.showInputDialog(null, "Moi ban nhap b :", JOptionPane.INFORMATION_MESSAGE);
        b = Double.parseDouble(stra2);
        stra3 = JOptionPane.showInputDialog(null, "Moi ban nhap c :", JOptionPane.INFORMATION_MESSAGE);
        c = Double.parseDouble(stra3);

        
        del = (b*b - 4*a*c);
        if(del < 0){
            JOptionPane.showMessageDialog(null, "Phuong trinh vo nghiem !");
            return;
        }
        else{
            if(del == 0) {
                x1 = -b/(2*a);
                x2 = x1;
                JOptionPane.showMessageDialog(null, "Phuong trinh co nghiem kep : x1 = x2 =" + x1);
            }
            else {
                x1 = (-b - Math.sqrt(del))/(2 * a);
                x2 = (-b + Math.sqrt(del))/(2 * a);
                JOptionPane.showMessageDialog(null, "Phuong trinh co 2 nghiem phan biet : x1 = " + x1 + " and x2 = " + x2);
            }
        }
    }
}

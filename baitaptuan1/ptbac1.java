import javax.swing.JOptionPane;

public class ptbac1 {
    public static void main(String[] args){
        String strNum1, strNum2;
        double Num1, Num2;
        strNum1 = JOptionPane.showInputDialog(null , "Pls input the 1st number :", JOptionPane.INFORMATION_MESSAGE);
        Num1= Double.parseDouble(strNum1);
        while(Num1 == 0) {
            JOptionPane.showMessageDialog(null, "The 1st number is invalid, pleae enter again !");
            strNum1 = JOptionPane.showInputDialog(null , "Pls input the 1st number:", JOptionPane.INFORMATION_MESSAGE);
            Num1 = Double.parseDouble(strNum1);
        }
        strNum2 = JOptionPane.showInputDialog(null , "Pls input the 2nd number :", JOptionPane.INFORMATION_MESSAGE);
        Num2= Double.parseDouble(strNum2);
        double result= -Num2/Num1;
        JOptionPane.showMessageDialog(null, "The result: " + result + "!");
    }
}
